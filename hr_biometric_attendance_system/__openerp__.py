{
    "name" : "Biometric Device Integration",
    "version" : "1.0",
    "author" : "Dash Open Source",
    "category" : "Custom",
    "website" : "www.dashopensource.com",
    'license': 'AGPL-3',
    'support':'dashopensource@gmail.com',
    'price':150,
    'currency':'USD',
    "description": "A Module for Biometric Device Integration",
    "depends" : ["base","hr"],
    "init_xml" : [],
    "data" : ["biometric_machine_view.xml","schedule.xml"],
    "active": False,
    "installable": True
}
