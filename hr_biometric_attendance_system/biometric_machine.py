# -*- coding: utf-8 -*-

from openerp import models,fields, api, _
from openerp.tools.translate import _
from datetime import datetime, timedelta
#from zklib import zklib
from zk import ZK, const
from openerp import exceptions
from openerp import tools




class hr_employee(models.Model):
	_name = 'hr.employee'
	_inherit = 'hr.employee'

	
	emp_code = fields.Char("Emp Code")
	category = fields.Char("category")
	#machine= fields.Many2one('biometric.machine',context="{'ref_name': 1}", options='{"always_reload": True}')
	
	@api.multi
	def synchronize_user_in_biometric_device(self):

		conn = None

		
		for x in self:
			machine_obj = self.env['biometric.machine'].search([('ref_name','=',x.machine)])

			machine_ip = machine_obj.name
			port = machine_obj.port

			zk = ZK(machine_ip, port=port, timeout=5, password=0, force_udp=False, ommit_ping=False)

			
			conn = zk.connect()

			conn.enable_device()
			conn.disable_device()

			users = conn.get_users()
			for user in users:
			   
				if user.user_id == x.emp_code:
					raise exceptions.Warning("User Already Exists")
				else:
					conn.set_user(uid=int(x.emp_code), name=x.name, privilege=const.USER_DEFAULT, password='', group_id='', user_id=x.emp_code, card=0)

			conn.enable_device()
			conn.disable_device()
		
			conn.disconnect()
		
	@api.model
	def _fill_device(self):
		models=self.env['biometric.machine'].search([])
		return [(x.ref_name,x.ref_name) for x in models]

	machine = fields.Selection(_fill_device, 'Device')


	@api.one
	def calculate_days(self):
		obj_hr_attendance = self.env['hr.attendance'].search([('employee_id','=',self.id)])

		if obj_hr_attendance:

			for x in obj_hr_attendance:

				from datetime import datetime

				import pytz



				local = pytz.timezone('Asia/Kolkata')

				display_date_result = datetime.strftime(pytz.utc.localize(datetime.strptime(x.name, tools.misc.DEFAULT_SERVER_DATETIME_FORMAT)).astimezone(local),"%d/%m/%Y %H:%M:%S")

				print display_date_result + '    ' + x.action

	


class biometric_machine(models.Model):
	_name= 'biometric.machine'
	
	name = fields.Char("Machine IP")
	ref_name = fields.Char("Location")
	port = fields.Integer("Port Number")
	address_id = fields.Many2one("res.partner", 'Working Address')
	company_id = fields.Many2one("res.company", "Company Name")
	atten_ids = fields.One2many('biometric.data', 'mechine_id', 'Attendance')
	
	@api.multi
	def download_attendance(self):
		conn = None
		machine_ip = self.name
		port = self.port
		zk = ZK(machine_ip, port=port, timeout=5, password=0, force_udp=False, ommit_ping=False)

		
		conn = zk.connect()

		conn.enable_device()
		conn.disable_device()
		attendance = conn.get_attendance()
		
		
		hr_attendance =  self.env["hr.attendance"]
		hr_employee = self.env["hr.employee"]
		biometric_data = self.env["biometric.data"]
		#raise exceptions.Warning(attendance)
		if (attendance):
			for lattendance in attendance:


				time_att = lattendance.timestamp
				
				atten_time1 = datetime.strptime(str(time_att), '%Y-%m-%d %H:%M:%S')
				

				atten_time = atten_time1 - timedelta(hours=5,minutes=30)
				atten_time = datetime.strftime(atten_time, '%Y-%m-%d %H:%M:%S')
				atten_time1 = datetime.strftime(atten_time1, '%Y-%m-%d %H:%M:%S')
				in_time = datetime.strptime(atten_time1, '%Y-%m-%d %H:%M:%S').time()

				time_new = str(in_time)
				time_new = time_new.replace(":", ".", 1)
				time_new = time_new[0:5]

				status = ''
				if lattendance.status == 1: 
					status ='FingerPrint' 
				else: 
					status = 'Face'

				check = ''
				att_check = ''
				if lattendance.punch == 0: 
					check='Check-In'
					att_check = 'sign_in' 
				else: 
					check = 'Check-Out'
					att_check = 'sign_out'

				employee_id = hr_employee.search([('emp_code','=',lattendance.user_id)]).id

				try:
					del_atten_ids = biometric_data.search([('emp_code', '=', lattendance.user_id), ('name', '=', atten_time)])
					if del_atten_ids:
						# hr_attendance.unlink(cr, uid, del_atten_ids)
						continue
					else:
						
						a = biometric_data.create({'name':atten_time, 'emp_code': lattendance.user_id, 'mechine_id': self._ids[0],'status':status,'punch_state':check})

						att = hr_attendance.create({'name':atten_time, 'employee_id': employee_id,'status':status,'action':att_check})
						
						print a
				except Exception, e:
					pass
					print "exception..Attendance creation======", e.args
		conn.enable_device()
		conn.disable_device()
	
		conn.disconnect()

			

	

	#Dowload attendence data regularly
	@api.model
	def schedule_download(self):

			scheduler_line_obj = self.env['biometric.machine']
			scheduler_line_ids = self.env['biometric.machine'].search([])
			for scheduler_line_id in scheduler_line_ids:
				scheduler_line = scheduler_line_id
				print scheduler_line
				try:
					scheduler_line.download_attendance()
				except:
					raise exceptions.Warning("Machine with %s is not connected" %(scheduler_line.name))

	@api.multi
	def clear_attendance(self):
		machine_ip = self.name
		port = self.port
		zk = ZK(machine_ip, port=port, timeout=5, password=0, force_udp=False, ommit_ping=False)
		if res:
			zk.enable_device()
			zk.disable_device()
			zk.clear_attendance()
			zk.enable_device()
			zk.disable_device()
			return True
		else:
			raise  exceptions.Warning("Unable to connect, please check the parameters and network connections.")


		


class biometric_data(models.Model):
	_name = "biometric.data"
	
	name = fields.Datetime('Date')
	emp_code = fields.Char('Employee Code')
	mechine_id = fields.Many2one('biometric.machine', 'Mechine No')
	status = fields.Char('Status')
	punch_state = fields.Char('Punch State')

	@api.one
	@api.depends('emp_code')
	def _get_user(self):
		obj_employee = self.env['hr.employee'].search([('emp_code','=',self.emp_code)])
		if obj_employee:

			self.user_name = obj_employee.name

	user_name = fields.Char(compute=_get_user)
	
class hr_attendance(models.Model):
	_inherit = 'hr.attendance'

	
	status = fields.Char("Status")

